
from pyvisa import ResourceManager
import time 
from csv import writer
import time 
from datetime import datetime
from PyQt5.QtWidgets import  QApplication, QWidget, QMessageBox, QFileDialog
from PyQt5.uic import loadUi
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5 import QtCore
import os
import sys


class SDPwriterClass(QWidget):
    def __init__(self):
        super().__init__()
        self.ui = loadUi('SPD.ui', self)
        self.setWindowModality(QtCore.Qt.ApplicationModal)       
        self.ui.Channel1Label.setChecked(True)
        self.ui.startPushButton.clicked.connect(self.evt_btnStart)
        self.ui.samplingTimeLineEdit.setText('0.1')
        self.ui.currentComboBox.addItem('0.2')
        self.ui.voltageComboBox.addItem('0.3')
        self.ui.UsbComboBox.addItems(['USB0::0xF4EC::0x1430::SPD3XHCD3R4464::INSTR'])
        self.ui.CSVradioButton.setChecked(True)
        self.ui.currentComboBox.setEditable(True)
        self.ui.voltageComboBox.setEditable(True)
        self.ui.UsbComboBox.setEditable(True)
        self.ui.outputDirPushButton.clicked.connect(self.output_folder_name)
        self.ui.progressBar.setValue(0)
        self.status_msg = 'Test sucessfully completed!'
    def evt_btnStart(self):
        if (self.ui.UsbComboBox.currentText() == 'Nil') or (self.ui.samplingTimeLineEdit.text() == 'Nil') or (self.ui.voltageComboBox.currentText() == 'Nil') \
        or (self.ui.currentComboBox.currentText() == 'Nil') or not (len(self.ui.outputFileNameLineEdit.text())> 0) or not (len(self.ui.outputDirLineEdit.text())>0):
            msg = QMessageBox()
            msg.setText("Please Enter all the values")
            msg.exec_()
        else:
            self.checkForDuplicateFile()
                   
    def checkForDuplicateFile(self):      
        self.FileName = self.ui.outputFileNameLineEdit.text()  
        try:
            suggestion = os.listdir(self.ui.outputDirLineEdit.text())
            suggestion=[x.split('.')[0] for x in suggestion]
            if not self.FileName in suggestion and len(self.FileName)>0:
                print('no file on common name exsist')
                self.threadOne = MyThread(obj = self)
                self.threadOne.start()
                self.threadOne.progressbar_Value.connect(self.event_update_progress_bar)
                self.threadOne.finished.connect(self.test_completed_msg_box)
                self.threadOne.instrument_status.connect(self.instrument_not_connected)

            elif self.FileName in suggestion:
                self.fileOverWriteAlert = QMessageBox()
                self.fileOverWriteAlert.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
                self.fileOverWriteAlert.setText('File :'+ '"'+self.FileName+'"' + 'alredy exist'+ '\n' + 
                'Do you want to append values to the existing file ?')
                ret = self.fileOverWriteAlert.exec()
                if ret == QMessageBox.Yes:
                    print('values will be appended to the exsisting file')
                    self.threadOne = MyThread(obj = self)
                    self.threadOne.start()
                    self.threadOne.progressbar_Value.connect(self.event_update_progress_bar)
                    self.threadOne.finished.connect(self.test_completed_msg_box)
                    self.threadOne.instrument_status.connect(self.instrument_not_connected)

                elif ret == QMessageBox.No:
                    print('No is clicked')  
        except FileNotFoundError:
            msg = QMessageBox()
            msg.setText('Invalid output folder path')
            msg.exec_()
                
    def output_folder_name(self):
        tempOutputDirPath = self.ui.outputDirLineEdit.text()
        OutputDirPath = QFileDialog.getExistingDirectory(self, 'Output Directory Path')
        if not OutputDirPath:
            self.ui.outputDirLineEdit.setText(tempOutputDirPath)
        else:
            self.ui.outputDirLineEdit.setText(OutputDirPath)

    def event_update_progress_bar(self, value):
        self.ui.progressBar.setValue(value)

    def test_completed_msg_box(self):
        msg = QMessageBox()
        msg.setText(self.status_msg)
        msg.exec_()
        self.ui.progressBar.setValue(0)
        self.status_msg = 'Test sucessfully completed!'
    def instrument_not_connected(self, status):
        if status is True:
            self.status_msg = 'Test Failed \n please Check the instrument connection'
        print('Check the instrument connection')


class MyThread(QThread):
    progressbar_Value = pyqtSignal(float)
    instrument_status = pyqtSignal(bool)
    def __init__(self, obj):
        super().__init__()
        self.ui = obj
        self.waitTime = 0.01
        self.list = ['index', 'channel_Number','time','measured_voltage','measured_current','measured_power']

    def run(self):
        from pyvisa.errors import VisaIOError
        try:
            self.readWriteVoltage()
            QApplication.sendPostedEvents()
        except VisaIOError:
            self.instrument_status.emit(True)
            '''alert = QMessageBox()
            alert.setText("Please check the instrument connection")
            alert.setStandardButtons(QMessageBox.Yes)
            alert.exec_()
            print('wrong usb address')
           # alert.exec_()
            #self.terminate()
            #QApplication.sendPostedEvents()'''
       
    def readWriteVoltage(self):
        '''Attributes: 
                        ChannelNumber(int): interger value to selct the channel number
                        channelVoltage(float): a float value for the channel voltage
                        channelCurrent(float): a float value for the channel currrent
                        samplingTime(flaot): a folat value '''   
        self.ResourceManger = ResourceManager()
        self.channelNumber = self.getChannelNumber()
        self.channelVotlage = float(self.ui.voltageComboBox.currentText())
        self.channelCurrent = float(self.ui.currentComboBox.currentText())
        self.samplingTime = float(self.ui.samplingTimeLineEdit.text())
        self.instanceAddress = self.ui.UsbComboBox.currentText()
        self.connectUsb = self.ResourceManger.open_resource(self.instanceAddress)
        self.connectUsb.write('CH'+str(self.channelNumber)+':VOLTage {:.1f}'.format((self.channelVotlage)))       
        self.connectUsb.query_delay = 0.01
        self.connectUsb.write_termination = '\n'
        self.connectUsb.read_termination = '\n'       
        time.sleep(self.waitTime)
        self.connectUsb.write('CH'+str(self.channelNumber)+':CURRent {:.1}'.format(self.channelCurrent))
        time.sleep(self.waitTime)        
        self.connectUsb.write( 'OUTPut' +' CH'+str(self.channelNumber)+',ON')
        
        #write the condition for text or csv file
        if self.ui.TxtradioButton.isChecked():
            self.writeTextFile()
        elif self.ui.CSVradioButton.isChecked():
            self.writeCSVFile()       
        elif self.ui.xlsxRb.isChecked():
            self.xlsx_writer()
        time.sleep(1.0)           
        self.connectUsb.write('CH'+ str(self.channelNumber)+':CURRent {:.1}'.format(0.0))
        self.connectUsb.write('CH'+str(self.channelNumber)+':VOLTage {:.1f}'.format((0.0)))
        self.connectUsb.write('OUTPut' +' CH'+ str(self.channelNumber)+',OFF')
    
    def writeTextFile(self):
        with open(self.ui.outputDirLineEdit.text()+'/'+self.ui.outputFileNameLineEdit.text()+'.txt', 'a',  newline='\n') as f_object:
            self.t_end = time.time() + 60 * float(self.ui.samplingTimeLineEdit.text())         
            f_object.write(self.list[0]+','+self.list[1]+',' + self.list[2]+','+self.list[3]+','+self.list[4]+','+ self.list[5]+'\n')                    
            for i, c, t, mv, mc, mp in self.get_volt_current_value():                       
                f_object.write(str(i)+','+str(c)+','+str(t)+','+str(mv)+','+str(mc)+','+str(mp)+'\n')
                self.progressbar_Value.emit(50)
            self.progressbar_Value.emit(100)         
                        
    def writeCSVFile(self):
        with open(self.ui.outputDirLineEdit.text()+'/'+self.ui.outputFileNameLineEdit.text()+'.csv', 'a',  newline='') as f_object:
            self.t_end = time.time() + 60 * float(self.ui.samplingTimeLineEdit.text())
            writer_object = writer(f_object, delimiter =",")
            
            writer_object.writerow(self.list)                  
            for i, c, t, mv, mc, mp in self.get_volt_current_value():                                                                            
                writer_object.writerow([i, c, t, mv, mc, mp])
                self.progressbar_Value.emit(50)                       
            self.progressbar_Value.emit(100)
                              
    def getChannelNumber(self):
        if self.ui.Channel1Label.isChecked():
            channle_Number = 1
        elif self.ui.Channel2Label.isChecked():
            channle_Number = 2
        return channle_Number
    
    def get_volt_current_value(self):
        self.index = 1
        channle_Number = self.getChannelNumber()
        while time.time() < self.t_end:                                             
            current_time = datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
            measured_voltage = self.connectUsb.query('MEAsure:VOLTage? '+'CH'+str(self.channelNumber))
            measured_voltage= measured_voltage.replace(',', '.')
            time.sleep(self.waitTime)
            measured_current =  self.connectUsb.query('MEAsure:CURRent? '+'CH'+str(self.channelNumber))
            measured_current= measured_current.replace(',', '.')
            measured_power = self.connectUsb.query('MEAsure:POWEr? '+'CH'+str(self.channelNumber))
            measured_power= measured_power.replace(',', '.')
            time.sleep(self.waitTime)
            List=[self.index, channle_Number, current_time, measured_voltage, measured_current, measured_power]
            yield List
            self.index += 1

    def xlsx_writer(self):
        from openpyxl import Workbook
        from openpyxl import load_workbook       
        self.t_end = time.time() + 60 * float(self.ui.samplingTimeLineEdit.text())   
        wb = Workbook()
        ws = wb.worksheets[0]
        ws.append(self.list)
        for i, c, t, mv, mc, mp in self.get_volt_current_value():           
            ws.append([i, c, t, mv, mc, mp]) 
            self.progressbar_Value.emit(50)
        wb.save(self.ui.outputDirLineEdit.text()+'/'+self.ui.outputFileNameLineEdit.text()+'.xlsx')
        self.progressbar_Value.emit(100)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = SDPwriterClass()   
    window.show()
    sys.exit(app.exec_())